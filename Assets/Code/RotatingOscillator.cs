﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Rotates an object in the given movement direction, the number of provided degrees, and then
 * oscillates back in the other direction the same amount. This movement repeats indefinitely
 * until paused.
 */
[DisallowMultipleComponent]
public class RotatingOscillator : MonoBehaviour
{
    protected bool Paused { get { return _paused; } set { _paused = value; } }
    [SerializeField]
    private bool _paused = false;

    [SerializeField]
    private Vector3 _movementDirection = new Vector3(0.0f, 0.0f, 0.0f);

    [SerializeField]
    private float _oscillationDuration = 5.0f;

    [SerializeField]
    private float _rotationAmountInDegrees = 0.0f;

    private float _rotationDirection = 1.0f;
    private float _elapsedRotationInDegrees = 0.0f;
    private float _speed;
    private Vector3 _initialEuler;


    // Start is called before the first frame update
    void Start()
    {
        _initialEuler = transform.eulerAngles;
        _speed = _rotationAmountInDegrees / _oscillationDuration;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        UpdateMovementFactor();

        var offsetDirection = _movementDirection * _elapsedRotationInDegrees;
        transform.eulerAngles = _initialEuler - offsetDirection;
    }

    private void UpdateMovementFactor()
    {
        if (!_paused && _oscillationDuration > Mathf.Epsilon)
        {
            _elapsedRotationInDegrees += _speed * Time.deltaTime * _rotationDirection;

            // flip directions when at the end
            if ((_elapsedRotationInDegrees >= _rotationAmountInDegrees) || (_elapsedRotationInDegrees <= Mathf.Epsilon))
            {
                FlipDirections();
            }
        }
    }

    public void TogglePause()
    {
        _paused = !_paused;
    }

    public void FlipDirections()
    {
        _rotationDirection *= 1;
    }
}