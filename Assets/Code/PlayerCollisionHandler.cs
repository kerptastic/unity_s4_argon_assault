﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionHandler : MonoBehaviour
{
    void OnCollisionEnter(Collision other)
    {
        //print("COLLISION");
    }

    void OnTriggerEnter(Collider other)
    {
        //print("TRIGGER");

        StartPlayerDeath();
    }


    private void StartPlayerDeath()
    {
        //SendMessage("EnableControls", false);
        SendMessage("ExplodeOnDeath");

    }
}
