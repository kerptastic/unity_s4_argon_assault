﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    private enum Direction
    {
        Up,
        Down,
        Left,
        Right,
        Neither
    }

    [Header("Movement")]
    [SerializeField]
    private float _movementSpeed = 35.0f;
    [SerializeField]
    private float _maxPitchInDegrees = 20.0f;
    [SerializeField]
    private float _maxRollInDegrees = 40.0f;
    [SerializeField]
    private float _rotationalSpeedInDegrees = 125.0f;
    [SerializeField]
    private float _levelOffSpeedInDegrees = 180.0f;

    [Header("Screen Space Correction")]
    [SerializeField]
    private float _yawCorrectionFactor = 0.0f;
    [SerializeField]
    private float _pitchCorrectionFactor = 0.0f;
    [SerializeField]
    private float _rollCorrectionFactor = 0.0f;

    [Header("Screen Clamping")]
    [SerializeField]
    private bool _enableRightLeftClamp = false;
    [SerializeField]
    private float _maxDeltaRightLeft = 0.0f;
    [SerializeField]
    private bool _enableUpDownClamp = false;
    [SerializeField]
    private float _maxDeltaUpDown = 0.0f;
    [SerializeField]
    private bool _enableForwardBackwardClamp = false;
    [SerializeField]
    private float _maxDeltaForwardBackward = 0.0f;


    //[SerializeField]
    private float _lastPitchDestination = 0.0f;
    //[SerializeField]
    private float _lastRollDestination = 0.0f;
    //[SerializeField]
    private float _currentPitchDestination = 0.0f;
    //[SerializeField]
    private float _currentRollDestination = 0.0f;
    //[SerializeField]
    private float _yawCounterBalance = 0.0f;
    //[SerializeField]
    private float _pitchCounterBalance = 0.0f;
    //[SerializeField]
    private float _rollCounterBalance = 0.0f;

    [Header("Weapons")]
    [SerializeField]
    private ParticleSystem _weaponSystemBullets;
    [SerializeField]
    private float _fireRate = 15.0f;

    private bool _areControlsEnabled = true;

    // Start is called before the first frame update
    void Start()
    {
        ValidateClampParameters();
    }

    // Update is called once per frame
    void Update()
    {
        if (_areControlsEnabled)
        {
            HandleInput();
        }
    }

    private void EnableControls(bool areEnabled)
    {
        _areControlsEnabled = areEnabled;
    }


    private void ValidateClampParameters()
    {
        _maxDeltaRightLeft = Mathf.Abs(_maxDeltaRightLeft);
        _maxDeltaUpDown = Mathf.Abs(_maxDeltaUpDown);
        _maxDeltaForwardBackward = Mathf.Abs(_maxDeltaForwardBackward);
    }


    private void HandleInput()
    {
        var leftXThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        var leftYThrow = CrossPlatformInputManager.GetAxis("Vertical");

        var rightXThrow = CrossPlatformInputManager.GetAxis("Horizontal-R");
        var rightYThrow = CrossPlatformInputManager.GetAxis("Vertical-R");

        var triggers = CrossPlatformInputManager.GetAxis("Triggers");


        CalculateRollDestination(-leftXThrow);
        CalculatePitchDestination(-leftYThrow);

        Rotate();
        Translate(leftXThrow, leftYThrow);

        //print(triggers);

        Aim(rightXThrow, rightYThrow);
        Fire(triggers);
    }


    private void Translate(float xThrow, float yThrow)
    {
        var horizontalMove = xThrow * Time.deltaTime * _movementSpeed;
        var verticalMove = yThrow * Time.deltaTime * _movementSpeed;

        Vector3 newPosition = this.gameObject.transform.localPosition + new Vector3(horizontalMove, verticalMove, 0.0f);
        this.gameObject.transform.localPosition = ClampMovement(newPosition);

        // this sets up all the minute counter rotations to compensate for the camera FOV causing skewed visuals
        // needs to happen after the ships location is updated and before rotations
        CalculateCounterBalances();
    }

    private void Rotate()
    {
        Quaternion currentLocalRotation = this.gameObject.transform.localRotation;
        Quaternion toRotation = Quaternion.Euler(_currentPitchDestination + -_pitchCounterBalance, _yawCounterBalance, _currentRollDestination + _rollCounterBalance);

        float speedThisFrame;

        if (_currentPitchDestination == 0.0f && _currentRollDestination == 0.0f)
        {
            speedThisFrame = _levelOffSpeedInDegrees;
        }
        else
        {
            speedThisFrame = _rotationalSpeedInDegrees;
        }

        this.gameObject.transform.localRotation =
            Quaternion.RotateTowards(currentLocalRotation, toRotation, speedThisFrame * Time.deltaTime);
    }

    private void Aim(float xThrow, float yThrow)
    {
        Quaternion currentLocalRotation = _weaponSystemBullets.transform.localRotation;
        Quaternion toRotation = Quaternion.Euler(-yThrow * 15.0f, xThrow * 15.0f, 0.0f);

        //_weaponSystemBullets.transform.localRotation = toRotation;

        _weaponSystemBullets.transform.localRotation =
            Quaternion.RotateTowards(currentLocalRotation, toRotation, 360.0f * Time.deltaTime);
    }

    private void Fire(float triggers)
    {
        var emissionSettings = _weaponSystemBullets.emission;

        // TODO: use a curve https://docs.unity3d.com/ScriptReference/ParticleSystem.MinMaxCurve.html

        //ParticleSystem.MinMaxCurve fireRateCurve = ParticleSystem.MinMaxCurve();

        if (triggers < 0.0f) //left
        {
            //_weaponSystemBullets.Play();
        }
        else if (triggers > 0.0f) //right
        {
            emissionSettings.rateOverTime = _fireRate * Mathf.Abs(triggers);
        }
        else
        {
            emissionSettings.rateOverTime = 0.0f;
        }

    }

    private Vector3 ClampMovement(Vector3 position)
    {
        var clampedX = position.x;
        var clampedY = position.y;
        var clampedZ = position.z;

        if (_enableRightLeftClamp)
        {
            clampedX = Mathf.Clamp(clampedX, -_maxDeltaRightLeft, _maxDeltaRightLeft);
        }

        if (_enableUpDownClamp)
        {
            clampedY = Mathf.Clamp(clampedY, -_maxDeltaUpDown, _maxDeltaUpDown);
        }

        if (_enableForwardBackwardClamp)
        {
            clampedZ = Mathf.Clamp(clampedZ, -_maxDeltaForwardBackward, _maxDeltaForwardBackward);
        }

        return new Vector3(clampedX, clampedY, clampedZ);
    }

    /*
     * Sets the counter balances, using a percentage of how far the ship moved across the screen
     * to determine how hard to counter balance.
     */
    private void CalculateCounterBalances()
    {
        Vector3 pos = this.gameObject.transform.localPosition;
        if (pos.x != Mathf.Epsilon)
        {
            _yawCounterBalance = pos.x * _yawCorrectionFactor;
        }
        if (pos.y != Mathf.Epsilon)
        {
            _pitchCounterBalance = pos.y * _pitchCorrectionFactor * Mathf.Abs(pos.y / _maxDeltaUpDown);
        }
        if (pos.z != Mathf.Epsilon)
        {
            _rollCounterBalance = pos.y * -_rollCorrectionFactor * Mathf.Abs(pos.x / _maxDeltaRightLeft);

            if (pos.x < Mathf.Epsilon)
            {
                _rollCounterBalance *= -1;
            }
        }
    }

    private void CalculateRollDestination(float xThrow)
    {
        _currentRollDestination = _maxRollInDegrees * xThrow;

        if (_lastRollDestination != _currentRollDestination)
        {
            _lastRollDestination = _currentRollDestination;
        }
    }

    private void CalculatePitchDestination(float yThrow)
    {
        _currentPitchDestination = _maxPitchInDegrees * yThrow;

        if (_lastPitchDestination != _currentPitchDestination)
        {
            _lastPitchDestination = _currentPitchDestination;
        }

    }
}
