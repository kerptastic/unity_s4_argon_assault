﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Collider _nonTriggerBoxCollider;
    private Scoreboard _scoreboard;

    [Header("Attributes")]
    [SerializeField]
    private uint _health = 100;

    private uint _currentHealth;

    [SerializeField]
    private uint _scoreValue = 1000;

    private uint DAMAGE_VALUE = 25;

    // Start is called before the first frame update
    void Start()
    {
        AddBoxCollider();

        _scoreboard = FindObjectOfType<Scoreboard>();

        _currentHealth = _health;
    }

    private void AddBoxCollider()
    {
        _nonTriggerBoxCollider = this.gameObject.AddComponent<BoxCollider>();
        _nonTriggerBoxCollider.isTrigger = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnParticleCollision(GameObject other)
    {
        uint score = TakeDamage(other);

        _scoreboard.IncrementScore(score);
    }

    private uint TakeDamage(GameObject weaponSystem)
    {
        _currentHealth -= DAMAGE_VALUE;

        if (_currentHealth <= 0)
        {
            SendMessage("OnDeath");
        }
        else
        {
            SendMessage("OnDamage");
        }

        return (uint)((float)_scoreValue * (float)DAMAGE_VALUE / (float)_health);
    }
}
