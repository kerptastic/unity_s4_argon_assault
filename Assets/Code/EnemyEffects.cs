﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEffects : MonoBehaviour
{
    [Header("General")]
    [SerializeField]
    Transform _hierarchyParentTransform;

    [Header("Effects")]
    [SerializeField]
    GameObject _deathExplosion;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnParticleCollision(GameObject other)
    {

    }

    private void OnDamage()
    {
        var explosion = Instantiate(_deathExplosion, this.gameObject.transform.position, Quaternion.identity);
        explosion.transform.parent = _hierarchyParentTransform;
    }

    private void OnDeath()
    {
        var explosion = Instantiate(_deathExplosion, this.gameObject.transform.position, Quaternion.identity);
        explosion.transform.parent = _hierarchyParentTransform;

        Destroy(this.gameObject);
    }
}
