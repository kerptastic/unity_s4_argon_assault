﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoreboard : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField]
    private ulong _minScore = 0;
    [SerializeField]
    private ulong _maxScore = 1;

    [Header("Player Score")]
    [SerializeField]
    private ulong _playerScore = 0;

    private Text _valueText;

    // Start is called before the first frame update
    void Start()
    {
        foreach (Text txt in GetComponentsInChildren<Text>())
        {
            if (txt.transform.tag == "ValueText")
            {
                _valueText = txt;
            }
        }

        _valueText.text = _playerScore.ToString();
    }

    void Update()
    {
        _valueText.text = _playerScore.ToString();
        _playerScore = Clamp(_playerScore);
    }

    public void IncrementScore(ulong amount)
    {
        _playerScore += amount;
        _playerScore = Clamp(_playerScore);

    }

    public void SetScore(ulong newScore)
    {
        _playerScore = newScore;
    }

    public void ResetScore()
    {
        _playerScore = 0;
    }

    private ulong Clamp(ulong current)
    {
        if (current <= _minScore)
            return _minScore;
        if (current >= _maxScore)
            return _maxScore;

        return current;
    }
}
