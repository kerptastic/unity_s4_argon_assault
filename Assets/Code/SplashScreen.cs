﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
    public enum Levels
    {
        Splash = 0, One = 1, Two = 2
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        HandleMenuInput();
    }

    private void HandleMenuInput()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            print("invoke");

            Invoke("LoadNewGame", 0.0f);
        }
    }

    private void LoadNewGame()
    {
        SceneManager.LoadScene(Levels.One.GetHashCode());
    }
}
