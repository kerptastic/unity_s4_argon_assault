﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruction : MonoBehaviour
{
    [SerializeField]
    private float _fuseTimeInSeconds = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, _fuseTimeInSeconds);
    }
}
